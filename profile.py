#!/usr/bin/python

"""
Use this profile to allocate Dense Deployment radios with associated compute, 
with the GNU Radio 3.8 software stack (and UHD 4.x libraries) installed.  
To see what radios are currently available, and what features/restrictions they
have, please check the POWDER map and radio information pages:

  [POWDER map](https://powderwireless.net/map)
  
  [Radio Information](https://www.powderwireless.net/radioinfo.php)

Note that these radios only have their "TX/RX" port connected!  The RX2 port
is the default receive port for many GNU Radio and UHD applications, but it
is not connected and so you will not see any signal on it.

If you plan to transmit anything, you will need to declare the frequency range
you plan to use for transmission via the parameters in this profile.  *If you
transmit without declaring frequencies, or transmit where you have not declared,
you will be in violation of the POWDER AUP and FCC regulations.*  Also be aware that
the attached antenna is matched for 3400 - 3800 MHz.  Do not try to transmit on
frequencies outside of this range.

**Note 06/30/22**: Dense Deployment radios will change somewhat in the coming months.
Currently each site has a ruggedized NUC-like compute node with attached 
(ruggedized) NI B210 radio.  As there is no compliance monitoring for these 
radios at this time, admin-reviewed reservations are required for using them.
Ultimately NI N310 radios will be deployed and the B210 radios will instead
run as transmission compliance monitors for these N310 radios.

Instructions:

This profile provides a base for doing your own development work with GNU
Radio and/or the Ettus UHD libraries.  This version of the profile provides
GNU Radio version 3.8, and version 4.0 of the UHD libraries on the compute nodes
associated with the radios.

You can SSH into the nodes to interact with them, and there is also a "Start X session"
option available for each node in the web UI for your experiment. The latter
lets you interact with a web-based X Windows session directly in your browser.

You can try running UHD FFT from the command line on one or more of your devices: 

```
uhd_fft -A TX/RX -g 65 -s 20M -f <freq>
```

This will start a UHD FFT process to view 20 MHz of spectrum centered at `<freq>` 
(don't include the angle brackets in the actual command).  It also tells
the program to set the gain at '65' (a reasonable value) and to use the "TX/RX" 
port for receiving ("RX2" is the default, which as decribed above won't work).
You can try, e.g., a frequency of 3550MHz to see some activity in the CBRS
band.

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.emulab.route as route
import geni.rspec.igext as ig

disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-GR38-PACK"
MIN_CBAND = 3350
MAX_CBAND = 3800

# List of Dense Deployment radios.
dense_radios = [
    ("cnode-wasatch",
     "Wasatch"),
    ("cnode-mario",
     "Mario"),
    ("cnode-moran",
     "Moran"),
    ("cnode-guesthouse",
     "Guesthouse"),
    ("cnode-ebc",
     "EBC"),
    ("cnode-ustar",
     "USTAR"),
]


# Frequency ranges to declare for this experiment
portal.context.defineStructParameter(
    "freq_ranges", "Frequency Ranges To Transmit In", [],
    multiValue=True,
    min=0,
    multiValueTitle="Frequency ranges to be used for transmission.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Range Min",
            portal.ParameterType.BANDWIDTH,
            3400.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Range Max",
            portal.ParameterType.BANDWIDTH,
            3410.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

# Set of Dense Deployment SFF+B210 devices to allocate
portal.context.defineStructParameter(
    "dense_radio_sites", "Dense Deployment Radio Sites", [],
    multiValue=True,
    min=1,
    multiValueTitle="Dense Deployment Compute+B210 radios to allocate.",
    members=[
        portal.Parameter(
            "radio",
            "Dense Deployment Site",
            portal.ParameterType.STRING,
            dense_radios[0], dense_radios,
            longDescription="Compute plus a B210 radio will be allocated for the selected site."
        ),
    ])

portal.context.defineParameter("alloc_shuttles", 
                               "Allocate all routes (mobile endpoints)",
                               portal.ParameterType.BOOLEAN, False)

portal.context.defineParameter("start_vnc", 
                               "Start X11 VNC on all compute nodes",
                               portal.ParameterType.BOOLEAN, True)

# Bind parameters
params = portal.context.bindParameters()

for i, frange in enumerate(params.freq_ranges):
    if frange.freq_min < MIN_CBAND or frange.freq_min > MAX_CBAND \
       or frange.freq_max < MIN_CBAND or frange.freq_max > MAX_CBAND:
        perr = portal.ParameterError("Dense site frequencies must be between %d and %d MHz" % (MIN_CBAND, MAX_CBAND), ["cband_freq_ranges[%d].freq_min" % i, "cband_freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["cband_freq_ranges[%d].freq_min" % i, "cband_freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

# Now verify the parameters.
portal.context.verifyParameters()

# Get request object
request = portal.context.makeRequestRSpec()

# Declare that we may be starting X11 VNC on the compute nodes.
if params.start_vnc:
    request.initVNC()

# Request frequency range(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 0)

# Request Compute + B210 radios at each Dense site
for rsite in params.dense_radio_sites:
    node = request.RawPC("%s-b210" % rsite.radio)
    node.component_id = rsite.radio
    node.disk_image = disk_image
    node.addService(rspec.Execute(shell="bash",
                                  command = "sudo patch /usr/bin/uhd_fft < /local/repository/uhd_fft.patch"))
    node.addService(rspec.Execute(shell="bash",
                                  command = "sudo patch /usr/lib/python3/dist-packages/gnuradio/uhd/uhd_siggen_base.py < /local/repository/uhd_siggen.patch"))
    if params.start_vnc:
        node.startVNC()

# Request ed1+B210 radio resources on all ME units (shuttles).
if params.alloc_shuttles:
    allroutes = request.requestAllRoutes()
    allroutes.disk_image = disk_image
    if params.start_vnc:
        allroutes.startVNC()

portal.context.printRequestRSpec()